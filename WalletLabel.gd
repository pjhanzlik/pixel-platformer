class_name WalletLabel
extends Label

func _on_player_coin_collected(value: int) -> void:
	text = (('%d' % (text.to_int() + value)).pad_zeros(3))


func _on_player_respawned() -> void:
	text = "000"
