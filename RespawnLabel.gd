extends Label

@onready var timer: Timer = $Timer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	text = ("%d" % ceil(timer.time_left)).pad_zeros(3)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	text = ("%d" % ceil(timer.time_left)).pad_zeros(3)


func _on_player_checkpointed(_body) -> void:
	timer.start()


func _on_player_respawned() -> void:
	timer.start()
