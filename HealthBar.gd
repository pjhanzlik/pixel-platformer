class_name HealthBar
extends TextureProgressBar

@export var hit_cooldown = 1
@onready var iframe_timer: Timer = $IframeTimer

signal died

func _on_player_hit(damage: float) -> void:
	value -= damage
	
	if value <= 0:
		died.emit()


func _on_player_checkpointed(_body) -> void:
	value = max_value


func _on_player_respawned() -> void:
	value = max_value


func _on_player_health_collected(amount) -> void:
	value += amount
