class_name Coin
extends AnimatedSprite2D

@onready var _spawn_tween: Tween = create_tween()
func _ready() -> void:
	_spawn_tween.finished.connect(queue_free)
	_spawn_tween.tween_property(self, "position", position - Vector2(0,18), 0.3).set_ease(Tween.EASE_OUT).set_trans(Tween.TRANS_SPRING)
