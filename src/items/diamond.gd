extends RigidBody2D

@onready var _spawn_tween: Tween = create_tween()

@export var value = 10

@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D

func _ready() -> void:
	_spawn_tween.tween_property(self, "position", position - Vector2(0,16), 0.5)
	_spawn_tween.finished.connect(func(): freeze = false; collision_shape_2d.disabled = false)
