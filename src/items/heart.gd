class_name Heart
extends CharacterBody2D


const SPEED = 16 / .5 * 2
const JUMP_VELOCITY = -400.0

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: int = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var _spawn_tween: Tween = create_tween()
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D

var is_moving_forward: bool = true
@export var amount = 1


func _ready() -> void:
	_spawn_tween.tween_property(self, "position", position - Vector2(0,16), 0.5)
	_spawn_tween.finished.connect(_on_spawn_tween_finished)

func _physics_process(delta: float) -> void:
	if not collision_shape_2d.disabled:
		# Add the gravity.
		if not is_on_floor():
			velocity.y += gravity * delta

		if is_on_wall():
			is_moving_forward = get_wall_normal().x > 0

		velocity.x = (1 if is_moving_forward else -1) * SPEED
		
		move_and_slide()

func _on_spawn_tween_finished() -> void:
	collision_shape_2d.disabled = false

