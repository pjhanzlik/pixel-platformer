class_name World
extends TileMap

@onready var point_query := PhysicsPointQueryParameters2D.new()

func _ready() -> void:
	point_query.collide_with_areas = true
	point_query.collide_with_bodies = false
	point_query.collision_mask = 2 ** 7

func bump(player: Player, body_rid: RID) -> void:
	if body_rid:
		var layer := get_layer_for_body_rid(body_rid)
		var coords := get_coords_for_body_rid(body_rid)
		var tile_data := get_cell_tile_data(layer, coords)
		var source_id := get_cell_source_id(layer, coords)
		var atlas_coords := get_cell_atlas_coords(layer, coords)
		var alternative_tile := get_cell_alternative_tile(layer, coords)
		if tile_data and tile_data.get_custom_data("breakable"):
			# Might not need exception...
			# player.add_exception_rid(body_rid)
			var packed_scene: PackedScene = tile_data.get_custom_data("spawn")
			if packed_scene:
				var spawn = packed_scene.instantiate()
				spawn.position = map_to_local(coords)
				call_deferred(&"add_child", spawn)
				player.respawned.connect(func(): if spawn != null: spawn.queue_free(), CONNECT_ONE_SHOT)
				
			var value: int = tile_data.get_custom_data("value")
			# Tiles with value respawn when player respawns
			if value:
				player.coin_collected.emit(value)
			# Other tiles respawn offscreen?

			var broken_tile: Vector4i = tile_data.get_custom_data("broken_tile_id")
			set_cell(layer, coords, broken_tile.x, Vector2(broken_tile.y, broken_tile.z), broken_tile.w)
			if broken_tile.x == -1 or broken_tile.w == -1 or broken_tile.y == -1 and broken_tile.z == --1:
				player.velocity.y = 0
			player.respawned.connect(func(): set_cell(layer, coords, source_id, atlas_coords, alternative_tile), CONNECT_ONE_SHOT)

func pipe(player: Player, collision: KinematicCollision2D) -> void:
	var layer := get_layer_for_body_rid(collision.get_collider_rid())
	var coords := get_coords_for_body_rid(collision.get_collider_rid())
	var tile_data = get_cell_tile_data(layer, coords)
	if tile_data != null:
		var normal: Vector2 = tile_data.get_custom_data("normal")
		var action: StringName = tile_data.get_custom_data("action")
		if normal != null and action != &"" and Input.is_action_pressed(action) \
		and collision.get_normal().dot(normal) > 0.1:
			for neighbor in [
				TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER,
				TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER,
				TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER,
				TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER,
				TileSet.CELL_NEIGHBOR_LEFT_SIDE,
				TileSet.CELL_NEIGHBOR_TOP_SIDE,
				TileSet.CELL_NEIGHBOR_RIGHT_SIDE,
				TileSet.CELL_NEIGHBOR_BOTTOM_SIDE
			]:
				if tile_data.get_terrain_peering_bit(neighbor) != -1:
					var target_coords = _calc_target_coords(coords, neighbor, layer)
					var target_tile_data = get_cell_tile_data(layer, target_coords)
					var target_normal = target_tile_data.get_custom_data("normal")
					if target_normal != Vector2(0,0):
						player.navigation_agent_2d.target_position = \
						map_to_local(Vector2(target_coords) + target_normal) \
						- target_normal * Vector2(tile_set.tile_size) / 2

						
						
						# Old version, better for short distances?
						var limit_rect := Rect2i(0, 0, 20000000, 810)

						var space_state := get_world_2d().direct_space_state
						point_query.position = player.navigation_agent_2d.target_position
						point_query.collision_mask = 2 ** 8
						var intersect_result = space_state.intersect_point(point_query, 1).pop_front()
						if intersect_result:
							var shape_owner: int = intersect_result.collider.shape_find_owner(intersect_result.shape)
							var shape_node: CollisionShape2D = intersect_result.collider.shape_owner_get_owner(shape_owner)
							limit_rect = shape_node.shape.get_rect()
							limit_rect.position = Vector2i(shape_node.global_position) - limit_rect.end

						# unlimit active camera based on travel direction
						var travel_vector = player.navigation_agent_2d.target_position - player.global_position
						var active_camera := get_viewport().get_camera_2d()
						
						if travel_vector.y < 0:
							active_camera.limit_top = limit_rect.position.y
						elif travel_vector.y > 0:
							active_camera.limit_bottom = limit_rect.end.y

						if travel_vector.x > 0:
							active_camera.limit_right = limit_rect.end.x
						elif travel_vector.x < 0:
							active_camera.limit_left = limit_rect.position.x
						
					return

func _calc_target_coords(coords: Vector2i, from: TileSet.CellNeighbor, layer: int) -> Vector2i:
	var tile_data = get_cell_tile_data(layer, coords)
	var neighbors = [
		TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER,
		TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER,
		TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER,
		TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER,
		TileSet.CELL_NEIGHBOR_LEFT_SIDE,
		TileSet.CELL_NEIGHBOR_TOP_SIDE,
		TileSet.CELL_NEIGHBOR_RIGHT_SIDE,
		TileSet.CELL_NEIGHBOR_BOTTOM_SIDE
	]
	
	# Erase the neighbor we came from to prevent backtracking
	match from:
		TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER:
			neighbors.erase(TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER)
		TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER:
			neighbors.erase(TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER)
		TileSet.CELL_NEIGHBOR_TOP_RIGHT_CORNER:
			neighbors.erase(TileSet.CELL_NEIGHBOR_BOTTOM_LEFT_CORNER)
		TileSet.CELL_NEIGHBOR_TOP_LEFT_CORNER:
			neighbors.erase(TileSet.CELL_NEIGHBOR_BOTTOM_RIGHT_CORNER)
		TileSet.CELL_NEIGHBOR_LEFT_SIDE:
			neighbors.erase(TileSet.CELL_NEIGHBOR_RIGHT_SIDE)
		TileSet.CELL_NEIGHBOR_TOP_SIDE:
			neighbors.erase(TileSet.CELL_NEIGHBOR_BOTTOM_SIDE)
		TileSet.CELL_NEIGHBOR_RIGHT_SIDE:
			neighbors.erase(TileSet.CELL_NEIGHBOR_LEFT_SIDE)
		TileSet.CELL_NEIGHBOR_BOTTOM_SIDE:
			neighbors.erase(TileSet.CELL_NEIGHBOR_TOP_SIDE)

	for neighbor in neighbors:
		if tile_data.get_terrain_peering_bit(neighbor) != -1:
			return _calc_target_coords(get_neighbor_cell(coords, neighbor), neighbor, layer)
	return coords
