extends TileMap

@onready var _tween: Tween = create_tween()

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_tween.tween_property(self, "position", position - Vector2(75,0), 2)
	_tween.tween_property(self, "position", position, 2)
	_tween.set_loops()
	_tween.set_process_mode(Tween.TWEEN_PROCESS_PHYSICS)
