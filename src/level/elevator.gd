class_name Elevator
extends TileMap

@export var velocity: Vector2 = Vector2(0,100)

func _physics_process(delta: float) -> void:
	position += velocity * delta
