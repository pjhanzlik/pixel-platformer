class_name Player
extends CharacterBody2D

@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
@onready var navigation_agent_2d: NavigationAgent2D = $NavigationAgent2D
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D
@onready var visible_on_screen_notifier_2d: VisibleOnScreenNotifier2D = $VisibleOnScreenNotifier2D
@onready var hurt_audio_stream_player_2d: AudioStreamPlayer2D = $HurtAudioStreamPlayer2D
@onready var checkpoint_audio_stream_player_2d: AudioStreamPlayer2D = $CheckpointAudioStreamPlayer2D
@onready var died_audio_stream_player_2d: AudioStreamPlayer2D = $DiedAudioStreamPlayer2D
@onready var coin_hurt_box: Area2D = $CoinHurtBox
@onready var ghost_shape_cast_2d: ShapeCast2D = $GhostShapeCast2D
@onready var hazard_hurt_box: Area2D = $HazardHurtBox
@onready var invincibility_timer: Timer = $InvincibilityTimer

# checkpoint ping threshold is the x distance in pixels a checkpoint must be
# from previous respawn location to play checkpoint audio
const CHECKPOINT_PING_THRESHOLD = 30

# TODO can hit and health_collected be consolidated into health_changed?
signal hit(damage: float)
signal health_collected(amount: float)

signal scored(int)
signal checkpointed(body: Node2D)
signal respawned()

@onready var _respawn_position: Vector2 = global_position

@export var max_speed: float = 200.0
@export var jump_velocity: float = -400.0
@export var dive_acceleration: float = 10.0
@export var damage: float = 0.4

var _hit_by_hazard: bool = false

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func despawn() -> void:
	collision_shape_2d.set_deferred(&"disabled", true)
	ghost_shape_cast_2d.clear_exceptions()
	died_audio_stream_player_2d.play()

func _physics_process(delta):
	if navigation_agent_2d.is_navigation_finished():
		# Add the gravity.
		velocity.y += gravity * delta
		if not is_on_floor():

			animated_sprite_2d.play("jump")
		else:
			animated_sprite_2d.play("default")

		# Handle Jump.
		if Input.is_action_just_pressed("jump") and is_on_floor():
			velocity.y = jump_velocity
		elif Input.is_action_just_released("jump") and velocity.y < 0:
			velocity.y /= 3
		
		if Input.is_action_pressed("crouch"):
			velocity.y += dive_acceleration

		# Get the input direction and handle the movement/deceleration.
		# As good practice, you should replace UI actions with custom gameplay actions.
		var direction = Input.get_axis("move_left", "move_right")
		if direction:
			velocity.x = direction * max_speed
			animated_sprite_2d.play()
			animated_sprite_2d.flip_h = direction > 0
		else:
			velocity.x = move_toward(velocity.x, 0, max_speed)
			animated_sprite_2d.stop()
		
		move_and_slide()
		
		if ghost_shape_cast_2d.is_colliding():
			for index in ghost_shape_cast_2d.get_collision_count():
				var ghost_collider := ghost_shape_cast_2d.get_collider(index)
				var ghost_rid := ghost_shape_cast_2d.get_collider_rid(index)
				
				if ghost_collider is World and velocity.y < 0.0:
					ghost_shape_cast_2d.add_exception_rid(ghost_rid)
					ghost_collider.bump(self, ghost_rid)
		
		if get_last_slide_collision():
			var collider = get_last_slide_collision().get_collider()

			
			# First check if the player can use a pipe, as movement is a priority
			if collider.has_method(&"pipe"):
				collider.pipe(self, get_last_slide_collision())
				if not navigation_agent_2d.is_navigation_finished():
					velocity = Vector2.ZERO
					animated_sprite_2d.scale = Vector2(0.66,0.66)
					collision_shape_2d.set_deferred("disabled", true)
					return
			elif collider is World:
				var collider_rid = get_last_slide_collision().get_collider_rid()

				var layer = collider.get_layer_for_body_rid(collider_rid)
				var coords = collider.get_coords_for_body_rid(collider_rid)
				
				var tile_damage = collider.get_cell_tile_data(layer, coords).get_custom_data(&"damage")
				if tile_damage:
					hit.emit(tile_damage)

			if is_on_wall() and collider.has_method(&"kick") and abs(get_last_slide_collision().get_remainder().x) > 0:
				collider.kick(self, get_last_slide_collision())

			elif is_on_floor() and collider.has_method(&"squish"):
				var bounce_velocity = collider.squish(self, get_last_slide_collision())
				if Input.is_action_pressed(&"jump"):
					velocity.y = bounce_velocity + jump_velocity
				else:
					velocity.y = bounce_velocity
			
			elif is_on_ceiling() and collider.has_method(&"bump"):
				collider.bump(self, get_last_slide_collision().get_collider_rid())
	else:
		# Move via navigation agent
		var next_path_position: Vector2 = navigation_agent_2d.get_next_path_position()
		var current_agent_position: Vector2 = global_position
		animated_sprite_2d.play("default")
		var new_velocity: Vector2 = (next_path_position - current_agent_position).normalized() * 3 * max_speed
		_on_velocity_computed(new_velocity)


func _on_velocity_computed(safe_velocity: Vector2):
	velocity = safe_velocity
	move_and_slide()


func _on_navigation_agent_2d_navigation_finished() -> void:
	collision_shape_2d.set_deferred("disabled", false)
	animated_sprite_2d.scale = Vector2.ONE


func _on_checkpoint_hurt_box_body_entered(body: Node2D) -> void:
	var is_new_checkpoint = abs(global_position.x - _respawn_position.x) \
	- CHECKPOINT_PING_THRESHOLD < 0
	
	_respawn_position = global_position
	checkpointed.emit(body)

	if not is_new_checkpoint:
		checkpoint_audio_stream_player_2d.play()


func _on_health_bar_died() -> void:
	despawn()


func _on_timer_timeout() -> void:
	despawn()


func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	var active_camera := get_viewport().get_camera_2d()
	
	if visible_on_screen_notifier_2d.global_position.y > active_camera.limit_bottom and navigation_agent_2d.is_navigation_finished():
		global_position = _respawn_position
		collision_layer |= 2 ** 1
		collision_shape_2d.disabled = false
		respawned.emit()

func _on_hit(hurt: float) -> void:
	if hurt <= 0:
		return
		
	hurt_audio_stream_player_2d.play()
	modulate = Color(Color.WHITE, 0.25)
	collision_mask ^= 2 ** 2
	collision_layer ^= 2 ** 1
	invincibility_timer.start()
	await invincibility_timer.timeout
	modulate = Color.WHITE
	collision_mask |= 2 ** 2
	collision_layer |= 2 ** 1

signal coin_collected(value: int)

func _on_coin_hurt_box_body_shape_entered(body_rid: RID, body: Node2D, _body_shape_index: int, _local_shape_index: int) -> void:
	if body is TileMap:
		var layer = body.get_layer_for_body_rid(body_rid)
		var coords = body.get_coords_for_body_rid(body_rid)
		var source_id = body.get_cell_source_id(layer, coords)
		var atlas_coords = body.get_cell_atlas_coords(layer, coords)
		var alternative_tile = body.get_cell_alternative_tile(layer, coords)
		var tile_data: TileData = body.get_cell_tile_data(layer, coords)
		var value = tile_data.get_custom_data("value")
		coin_collected.emit(value)
		
		# Erase coin tile, so only one player can have it
		body.call_deferred(&"set_cell", layer, coords)
		# On player respawn, respawn coin tile
		respawned.connect(func(): body.call_deferred(&"set_cell", layer, coords, source_id, atlas_coords, alternative_tile), CONNECT_ONE_SHOT)
	elif body.get(&"value") is int:
		coin_collected.emit(body.value)
		body.queue_free()

func _on_heart_hurt_box_body_entered(body: Node2D) -> void:
	if body.get(&"amount"):
		health_collected.emit(body.amount)
		body.queue_free()


func _on_bump_hurt_box_body_shape_entered(body_rid: RID, body: Node2D, _body_shape_index: int, _local_shape_index: int) -> void:
		if body.has_method(&"bump") and velocity.y < 0:
			body.bump(self, body_rid)
			velocity.y = 0


func _on_hazard_hurt_box_body_shape_entered(body_rid: RID, body: Node2D, _body_shape_index: int, _local_shape_index: int) -> void:
	if body is TileMap:
		var layer: int = body.get_layer_for_body_rid(body_rid)
		var coords: Vector2i = body.get_coords_for_body_rid(body_rid)
		var tile_damage: float = body.get_cell_tile_data(layer,coords).get_custom_data(&"damage")
		if tile_damage and not _hit_by_hazard:
			_hit_by_hazard = true
			hit.emit(tile_damage)
			await invincibility_timer.timeout
			_hit_by_hazard = false


func _on_hazard_hurt_box_area_entered(area: Area2D) -> void:
	if area.get(&"damage"):
		hit.emit(area.damage)


func _on_hazard_hurt_box_body_entered(body: Node2D) -> void:
	if body.get(&"damage"):
		hit.emit(body.damage)
