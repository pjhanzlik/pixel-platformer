class_name CameraLimitArea2D
extends Area2D

func _on_body_shape_entered(_body_rid: RID, player: Node2D, _body_shape_index: int, local_shape_index: int) -> void:
	if player is Player:
		var local_shape_owner: int = shape_find_owner(local_shape_index)
		var local_shape_node: CollisionShape2D = shape_owner_get_owner(local_shape_owner)
		
		var limit_rect: Rect2i = local_shape_node.shape.get_rect()
		limit_rect.position = Vector2i(local_shape_node.global_position) - limit_rect.end

		if player:
			var active_camera: Camera2D = get_viewport().get_camera_2d()
			
			
			active_camera.limit_top = limit_rect.position.y
			active_camera.limit_left = limit_rect.position.x
			active_camera.limit_bottom = limit_rect.end.y
			active_camera.limit_right = limit_rect.end.x

func _on_body_exited(body: Node2D) -> void:
	if body is Player or body.get(&"visible_on_screen_enabler_2d"):
		return
		
	if body.has_method(&"despawn"):
		body.call_deferred(&"despawn")
		return
	
	if body is TileMap:
		return
	
	if body != null and not body.is_queued_for_deletion():
		body.queue_free()


func _on_area_shape_exited(_area_rid: RID, area: Area2D, _area_shape_index: int, local_shape_index: int) -> void:
	if area.owner is Elevator:
		var local_shape_owner := shape_find_owner(local_shape_index)
		var local_shape_node := shape_owner_get_owner(local_shape_owner)
		if area.owner.velocity.y < 0:
			area.owner.global_position.y = local_shape_node.shape.get_rect().end.y + local_shape_node.global_position.y
		else:
			area.owner.global_position.y = local_shape_node.shape.get_rect().position.y + local_shape_node.global_position.y


func _on_area_exited(area: Area2D) -> void:
	if not area.owner is Elevator:
		area.queue_free()
