extends Area2D

var tween: Tween
@onready var _spawn_position := position
@onready var timer: Timer = $Timer
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D

func _on_body_entered(body: Node2D) -> void:
	if body.has_signal(&"hit"):
		body.hit.emit(1.0)


func _on_visible_on_screen_enabler_2d_screen_exited() -> void:
	if tween:
		tween.kill()
	position = _spawn_position
	collision_shape_2d.disabled = true

func _on_visible_on_screen_enabler_2d_screen_entered() -> void:
	timer.start()


func _on_timer_timeout() -> void:
	tween = create_tween()
	collision_shape_2d.disabled = false
	tween.set_loops()
	tween.tween_property(self, "position", Vector2(position.x,position.y - 18), 2)
	tween.tween_property(self, "position", Vector2(position.x,position.y), 4)
