extends CharacterBody2D

@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D
@onready var audio_stream_player_2d: AudioStreamPlayer2D = $AudioStreamPlayer2D
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D

@export var speed := 25.0
@export var mitigation := 0.0
@export var damage := 0.5
@export var bounce_velocity := -160

var _speed_boost := 0.0
var _speed := 0.0
var _kicker: Player

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta
	
	var slide_collision_count = get_slide_collision_count()
	for slide_index in range(slide_collision_count):
		var collision = get_slide_collision(slide_index)
		var collider = collision.get_collider()
		
		# Hitting left or right
		if collision.get_normal().dot(Vector2.DOWN) <= 0.1 and _speed_boost != 0:
			if collider.has_method(&"take_damage"):
				if not collider.take_damage(damage):
					despawn()
			elif collider.has_method(&"bump") and _kicker:
				collider.bump(_kicker, collision.get_collider_rid())
	
	if is_on_wall() and get_last_slide_collision():
		if get_last_slide_collision().get_collider() is TileMap \
		or animated_sprite_2d.animation == "walk":
			animated_sprite_2d.flip_h = get_last_slide_collision().get_normal().x > 0
	
	var direction = 1 if animated_sprite_2d.flip_h else -1
	match animated_sprite_2d.animation:
		"idle":
			velocity.x = direction * _speed_boost
		"walk":
			velocity.x = direction * _speed 
	
	move_and_slide()
	if get_last_slide_collision() and get_last_slide_collision().get_collider() is Player \
	and get_last_slide_collision().get_angle(Vector2.UP) < floor_max_angle:
		get_last_slide_collision().get_collider().hit.emit(damage)

func take_damage(amount: float) -> bool:
	if amount > mitigation:
		despawn()
		return true
	else:
		return false


func despawn() -> void:
	audio_stream_player_2d.play()
	velocity.y -= 10
	collision_shape_2d.set_deferred(&"disabled", true)

func squish(_squisher: Player, _collision: KinematicCollision2D) -> float:
	audio_stream_player_2d.play()
	_speed_boost = 0
	animated_sprite_2d.animation = "idle"
	_kicker = null
	return bounce_velocity

func kick(kicker: Player, collision: KinematicCollision2D) -> void:
	if animated_sprite_2d.animation == "idle" and _speed_boost == 0:
		_speed_boost = kicker.max_speed
		_kicker = kicker
		animated_sprite_2d.flip_h = collision.get_normal().x < 0
	elif animated_sprite_2d.animation == "walk" \
	# Don't damage player if behind a kicked crawler
	or collision.get_normal().x > 0 == animated_sprite_2d.flip_h:
		kicker.hit.emit(damage)

# Let the mob have a break when offscreen
func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	# When the mob is offscreen and has no collision we can get rid of it
	if collision_shape_2d.disabled:
		queue_free()


func _on_visible_on_screen_notifier_2d_screen_entered() -> void:
	_speed = speed
