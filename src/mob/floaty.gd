extends CharacterBody2D


@export var speed = 100.0
@export var flap_velocity = -200.0
@export var damage = 0.5
@export var bounce_velocity = -200.0
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D

# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: int = ProjectSettings.get_setting("physics/2d/default_gravity")

func squish(_squisher: Player, _collision: KinematicCollision2D) -> float:
	set_physics_process(false)
	animated_sprite_2d.pause()
	return bounce_velocity

func _process(delta: float) -> void:
	if not is_physics_processing():
		velocity.y += gravity * delta
		position += velocity * delta
		
func _physics_process(delta: float) -> void:
	# Add the gravity.
	velocity.y += gravity * delta * 0.5
	velocity.x = -speed

	if get_last_slide_collision() \
	and get_last_slide_collision().get_normal().dot(Vector2.DOWN) <= 0.1 \
	and get_last_slide_collision().get_collider().has_signal(&"hit"):
		get_last_slide_collision().get_collider().hit.emit(damage)

	move_and_slide()


func _on_flap_timer_timeout() -> void:
	if is_physics_processing():
		velocity.y += flap_velocity
