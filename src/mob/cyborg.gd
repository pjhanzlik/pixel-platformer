extends CharacterBody2D


const SPEED = 50.0
const JUMP_VELOCITY = -400.0
const damage = 1.0


# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity: int = ProjectSettings.get_setting("physics/2d/default_gravity")
var _direction: int = 0

@onready var visible_on_screen_enabler_2d: VisibleOnScreenEnabler2D = $VisibleOnScreenEnabler2D
@export var weapon: PackedScene
@onready var weapon_marker_2d: Marker2D = $WeaponMarker2D

func _physics_process(delta: float) -> void:
	# Add the gravity.
	if not is_on_floor():
		velocity.y += gravity * delta

	# Handle Jump.
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = JUMP_VELOCITY

	if _direction:
		velocity.x = _direction * SPEED
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)

	move_and_slide()


func _on_weapon_timer_timeout() -> void:
	var weapon_spawn = weapon.instantiate()
	weapon_spawn.global_position = weapon_marker_2d.global_position
	get_tree().root.add_child(weapon_spawn)


func _on_direction_timer_timeout() -> void:
	_direction = randi_range(-1,1)
