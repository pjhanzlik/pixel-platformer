extends CharacterBody2D

@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D
@onready var collision_shape_2d: CollisionShape2D = $CollisionShape2D
@onready var left_ray_cast_2d: RayCast2D = $LeftRayCast2D
@onready var right_ray_cast_2d: RayCast2D = $RightRayCast2D
@onready var visible_on_screen_notifier_2d: VisibleOnScreenNotifier2D = $VisibleOnScreenNotifier2D

@export var damage: float = 0.5
@export var mitigation: float = 0.0
@export var bounce_velocity: float = 0.0

const SPEED: float = 33.0
var _entered_screen: bool = false
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

func despawn() -> void:
	if visible_on_screen_notifier_2d.is_on_screen():
		collision_shape_2d.disabled = true
	else:
		queue_free()

func _physics_process(delta: float) -> void:
	# Add the gravity.
	velocity.y += gravity * delta
	
	if _entered_screen:
		# Assume animated_sprite_2d faces left by default
		if is_on_wall():
			animated_sprite_2d.flip_h = get_wall_normal().x > 0
		
		velocity.x = SPEED * (1 if animated_sprite_2d.flip_h else -1)

		# Change direction when about to fall of a ledge
		if is_on_floor() and not (left_ray_cast_2d.is_colliding() and right_ray_cast_2d.is_colliding()):
			animated_sprite_2d.flip_h = not animated_sprite_2d.flip_h
			velocity.x *= -1
		
	move_and_slide()
	if get_last_slide_collision():
			var collider := get_last_slide_collision().get_collider()
			if collider.has_signal(&"hit"):
				collider.hit.emit(damage)

func take_damage(amount: float) -> bool:
	if amount > mitigation:
		despawn()
		return true
	else:
		return false

func squish(squisher: Player, _collision: KinematicCollision2D) -> float:
	squisher.hit.emit(damage)
	return bounce_velocity

func kick(kicker: Player, _collision: KinematicCollision2D) -> void:
	kicker.hit.emit(damage)

func _on_visible_on_screen_notifier_2d_screen_entered() -> void:
	_entered_screen = true


func _on_visible_on_screen_notifier_2d_screen_exited() -> void:
	if(collision_shape_2d.disabled):
		queue_free()
