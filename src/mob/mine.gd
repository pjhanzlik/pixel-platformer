extends Area2D

@export var damage := 0.5
@export var velocity := Vector2(-50.0,0.0)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta: float) -> void:
	position += velocity * delta
