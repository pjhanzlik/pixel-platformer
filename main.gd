extends Node2D

@onready var timer: Timer = $Timer
@onready var timer_texture_rect: TextureRect = $CanvasLayer/Control/TimerTextureRect
@onready var player: CharacterBody2D = $Player

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta: float) -> void:
	timer_texture_rect.time = timer.time_left


func _on_timer_timeout() -> void:
	player.take_damage(player.health)
